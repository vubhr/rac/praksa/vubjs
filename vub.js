class Vub {
    #projekt = "p_common";
    key = 1;
    #url = 'https://uat.vub.zone/praksa/router.php'
    schema;
    data;
    schema = ''
    data = ''
    parent = "#container";
    idata;

    constructor(schema, data) {
        this.schema = schema;
        this.data = data;
    }

    login(idata) {
        //kontrole za login
        console.log("U loginu sam")
        this.idata = idata;
        console.log(this.#getData(idata));
    }

    change(ID) {
        var objekt = this.data.find(t => t.ID === ID)
        delete objekt.ACTION;
    }

    trash(ID) {
        var objekt = this.data.find(t => t.ID === ID)
        delete objekt.ACTION;
    }



    #getData(idata) {
        if (idata == null) {
            //idata = { "projekt": this.#projekt, "procedura": this.schema.call.procedura };
            //idata = { "projekt": 'p_eljuga', "procedura": 'p_get_data', "username":"meljuga@gmail.com", "password":"123"};
            idata = { "projekt": 'p_eljuga', "procedura": 'p_get_data', "tablica":"PREGLEDI_V" };
        } else {
            if (idata === 'A') {
                idata = { "projekt": 'p_eljuga', "procedura": 'p_get_data', "tablica":"PREGLEDI_V" };
            }
        }

        var jsonBody;
        console.log("ulazni podaci", idata);
        $.ajax({
            type: 'POST',
            url: this.#url,
            data: idata,
            success: function (podaci) {
                jsonBody = JSON.parse(podaci);
                console.log(podaci);
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            async: false
        });
        return jsonBody.data;
    }


    tablica() {
        this.data = this.#getData();
        this.data.forEach(element => element.ACTION = '<i class="fas fa-edit zelena" onclick=\"(function(){vub.change(' + element.ID + ');})()\" ></i>&nbsp;&nbsp;&nbsp;' +
            '<i class="fa fa-trash crvena" onclick=\"(function(){vub.trash(' + element.ID + ');})()\"></i>');

        $(this.schema.tablica.html, this.schema.tablica.atrb).appendTo(this.parent);
        $("#" + this.schema.tablica.atrb.id).DataTable({
            data: this.data,
            "columns": this.schema.tablica.config
        })
    }

    #input(objekt, key) {
        $('<div>')
            .attr('class', "control-group")
            .appendTo("#forma");

        $('<label>')
            .attr('class', "control-label")
            .text(objekt.label)
            .appendTo('div.control-group:last');

        $('<div>')
            .attr('class', "controls")
            .appendTo('div.control-group:last');

        $(objekt.html, objekt.atrb)
            .attr('id', key)
            .attr('name', key)
            .appendTo('div.controls:last');
    }

    #radio(objekt, key) {
        $('<div>')
            .attr('class', "control-group")
            .appendTo("#forma");

        $('<label>')
            .attr('class', "control-label")
            .text(objekt.label)
            .appendTo('div.control-group:last');

        $('<div>')
            .attr('class', "controls")
            .appendTo('div.control-group:last');

        Object.keys(objekt.data).forEach(function (i) {
            $('<label>')
                .attr('class', "radio")
                .text(objekt.data[i])
                .appendTo('div.controls:last');
            $(objekt.html, objekt.atrb)
                .attr('value', i)
                .attr('id', i)
                .attr('name', key)
                .appendTo('label.radio:last');
        })
    }

    #button(objekt, key) {
        console.log(objekt, "i", key, this.schema, this.data)
        var schema = this.schema;
        var data = this.data;


        Object.keys(this.schema[key]).forEach((index) => {
            $('<div>')
                .attr('class', "control-group")
                .appendTo("#forma");
            $('<div>')
                .attr('class', "controls")
                .appendTo('div.control-group:last');

            $(this.schema.action[index].html, this.schema.action[index].atrb)
                .attr('id', index)
                .attr('class', "btn")
                .click(function () { scrapeData(schema, data) })
                .appendTo('div.controls:last');
        });

    }

    #select(objekt, key) {
        $('<div>')
            .attr('class', "control-group")
            .appendTo("#forma");

        $('<label>')
            .attr('class', "control-label")
            .text(objekt.label)
            .appendTo('div.control-group:last');

        $('<div>')
            .attr('class', "controls")
            .appendTo('div.control-group:last');

        $(objekt.html, objekt.atrb)
            .attr('id', key)
            .attr('name', key)
            .appendTo('div.controls:last');

        //fixed values, For example Gender
        if ('data' in objekt) {
            Object.keys(objekt.data).forEach(function (i) {
                $('<option>')
                    .attr('value', i)
                    .text(objekt.data[i])
                    .appendTo("#" + key)
            })
        } else {
            //getting data
            var response = this.#getData();
            $.each(response, function (k, v) {
                $('<option>')
                    .attr('value', v.ID)
                    .text(v.NAZIV) //trebalo bi biti NAZIV kada se pokrenu viewovi na bazi za sada je fiksno ZUPANIJA
                    .appendTo("#" + key);
            });
        }
    }

    #datalist(objekt, key) {
        $('<div>')
            .attr('class', "control-group")
            .appendTo("#forma");

        $('<label>')
            .attr('class', "control-label")
            .text(objekt.label)
            .appendTo('div.control-group:last');

        $('<div>')
            .attr('class', "controls")
            .appendTo('div.control-group:last');

        $('<input>')
            .attr('type', "text")
            .attr('list', key)
            .attr('name', key)
            .appendTo('div.controls:last');

        $('<datalist>')
            .attr('id', key)
            .appendTo('div.controls:last');

        var response = this.#getData();
        $.each(response, function (k, v) {
            $('<option>')
                //.attr('value', v.ID) //trebalo bi biti NAZIV kada se pokrenu viewovi na bazi za sada je fiksno ZUPANIJA
                .text(v.NAZIV)
                
                .attr('data-value', v.ID)
                .appendTo("#" + key);
        });
      //$("input[name=search]").val()

        /*
        $(objekt.html, objekt.atrb)
            .attr('id', key)
            .attr('name', key)
            .appendTo('div.controls:last');

        //fixed values, For example Gender
        if ('data' in objekt) {
            Object.keys(objekt.data).forEach(function (i) {
                $('<option>')
                    .attr('value', i)
                    .text(objekt.data[i])
                    .appendTo("#" + key)
            })
        } else {
            //getting data
            var response = this.#getData();
            $.each(response, function (k, v) {
                $('<option>')
                    .attr('value', v.ID)
                    .text(v.ZUPANIJA) //trebalo bi biti NAZIV kada se pokrenu viewovi na bazi za sada je fiksno ZUPANIJA
                    .appendTo("#" + key);
            });
        }*/
    }

    //generira formu na temelju schemae
    forma(objekt) {
        var parent = '#container';
        var formSetting = {
            "class": "form-horizontal row-fluid",
            "method": "GET",
            "action": "#",
            "id": "forma",
            "onsubmit": "return mySubmitFunction(event)"
        }
        $('<form>', formSetting).appendTo(parent); //appendam u #container, a poslije u #forma
        parent = '#forma';

        Object.keys(this.schema).forEach((key) => {
            //console.log(schema[key], key); //{label: 'Korisničko ime', html: '<input>', atrb: {…}} 'email'
            if (key != 'action') {
                if (key != 'call') {
                    if (schema[key].atrb.type != 'radio') {
                        if (schema[key].atrb.type != 'select') {
                            if (schema[key].html == '<input>') {
                                this.#input(schema[key], key);
                            }
                            if (schema[key].html == '<datalist>') {
                                console.log(schema[key], ' i ', key)
                                this.#datalist(schema[key], key);
                            }

                        } else {//select
                            this.#select(schema[key], key)
                        }
                    } else {//radio
                        this.#radio(schema[key], key)
                    }

                } else {
                    //preskačem naziv tablice, call svojstvo
                }
            } else {
                this.#button(schema[key], key)
            }
        });
    }
}


//const vub = new Vub(schema, data);

//vub.tablica ();

const vub = new Vub(schema, data);
//console.log("schema", schema, "data", data)
//vub.login('A');
vub.forma();


//priprema i šalje request prema servisu
function sendData(schema, data) {
    console.log("Poslao", data);
}

//dohvaća podatke s forme na temelju ID-a i keya iz data
function scrapeData(schema, data) {
    Object.keys(data).forEach(function (key) {
        var tip = schema[key].atrb.type;
        if (tip === "radio") {
            var checked = "input[name='" + key + "']:checked";
            data[key] = $(checked).val();
        } else {
            data[key] = $('#' + key).val();
        }
    });
}


function menuHandler(k) {
    $("#container").empty();
    vub.schema = {
        "call": {
            "procedura": "p_get_data",
            "table": "PREGLEDI_V"
        },
        "tablica": {
            "html": "<table>",
            "atrb": {
                "id": "example",
                "class": "display",
                "style": "width:80%"
            },
            "config":
                [
                    {
                        "data": "ID",
                        "title": "ID",
                        "searchable": false,
                        "visible": false
                    },
                    {
                        "data": "PREGLED",
                        "title": "Pregled"
                    },
                    {
                        "data": "FIKSNI",
                        "title": "Fiksni"
                    },
                    {
                        "data": "VARIJABILNI",
                        "title": "Varijabilni"
                    },
                    {
                        "data": "DIREKTNI",
                        "title": "Direktni"
                    },
                    {
                        "width": "5%",
                        "data": "ACTION",
                        "title": "action",
                        "searchable": false,
                        "orderable": false
                    }
                ]
        }
    };
    vub.data = [];
    vub.tablica();
    return false;
}



function mySubmitFunction(event) {
    sendData(schema, data);
    //dodatna provjera na podatke
    console.log("zvao");
    return false;
}
