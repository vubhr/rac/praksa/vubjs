schema = {
    "call": {
        "procedura": "p_zupanije",
        "table": "KORISNIK"
    },
    "email": {
        "label": "Korisničko ime:",
        "html": "<input>",
        "atrb": {
            "type": "text",
            "required": false,
            "size": 60,
            "width": "50%",
        }
    },
    "ime": {
        "label": "Ime korisnika:",
        "html": "<input>",
        "atrb": {
            "type": "text",
            "required": true,
            "maxlength": "10",
            "width": "40%",
        }
    },
    "prezime": {
        "label": "Prezime korisnika:",
        "html": "<input>",
        "atrb": {
            "type": "text",
            "required": true,
            "maxlength": "10",
            "width": "90%",
        }
    },
    "password": {
        "label": "Zaporka:",
        "html": "<input>",
        "atrb": {
            "type": "password",
            "required": true,
            "size": 60,
            "width": "10%"
        }
    },
    "zupanije": {
        "label": "--Odaberite županiju--",
        "html": "<select>",
        "atrb": {
            "type": "select",
            "required": true,

        },
        "call": {
            "procedura": "p_zupanije",
            "table": "ZUPANIJE_V",
            "async": false,
            "wait": 0
        }
    },
    "spol": {
        "label": "--Odaberite spol--",
        "html": "<select>",
        "atrb": {
            "type": "select",
            "required": true,

        },
        "data": {
            "0": "muško",
            "1": "žensko",
            "2": "nemam pojma"
        }
    },
    "boja": {
        "label": "Odaberi boju:",
        "html": "<input>",
        "atrb": {
            "type": "radio",
            "required": true,

        },
        "data": {
            "0": "bijela",
            "1": "crna",
            "2": "zelena",
            "3": "crvena",
            "4": "pinkiš"
        }
    },
    "action": {
        "cancel": {
            "html": "<input>",
            "atrb": {
                "type": "submit",
                "value": "Pošalji",
            }
        }
    }
}