schema = {
    "call": {
        "procedura": "p_zupanije",
        "table": "ZUPANIJE_V"
    },
    "tablica": {
        "html": "<table>",
        "atrb": {
            "id": "example",
            "class": "display",
            "style":"width:60%"
        },
        "config":
            [
                {
                    "data": "ID",
                    "title":"ID",
                    "searchable": false,
                    "visible":false
                },
                {
                    "data": "ZUPANIJA",
                    "title": "Županija"
                },
                {
                    "width":"5%",
                    "data":"ACTION",
                    "title": "action",
                    "searchable": false,
                    "orderable":false
                }
            ]
    }
}